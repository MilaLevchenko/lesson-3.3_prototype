/**
 * Created by Людмила on 22.03.2017.
 */
//1.Создать конструктор Student для объекта студента, который принимает имя и кол-во баллов в качестве аргумента.
// Все объекты созданные этим конструктором должны иметь общий метод show, который работает так же как в предыдущем домашнем задании.

function Student (name, point) {
  this.name = name;
  this.point = point;
}
Student.prototype.show = function () {
  console.log("Студент %s набрал %d баллов", this.name, this.point);
}

//2.Создать конструктор StudentList, который в качестве аргумента принимает название группы и массив формата studentsAndPoints из задания по массивам.
// Массив может отсутствовать или быть пустым.
// Объект, созданный этим конструктором должен обладать всеми функциями массива.
// А так же иметь метод add, который принимает в качестве аргументов имя и кол-во баллов, создает нового студента и добавляет его в список.


function StudentList (groupName, groupArr) {
  this.groupName = groupName;
  if (groupArr) {

    for(var i = 0; i < groupArr.length; i+=2){
      this.push(new Student(groupArr[i], groupArr[i+1]));
    };
    /*for (var i = 0, imax = groupArr.length; i < imax; i += 2) {
      var student = new Student(groupArr[i], groupArr[i + 1]);
      this.push(student);} короче запись*/
    }
}

StudentList.prototype = Object.create(Array.prototype);
StudentList.prototype.add = function (name, point) {
  this.push(new Student(name, point))
};

//3.Создать список студентов группы «HJ-2» и сохранить его в переменной hj2. Воспользуйтесь массивом studentsAndPoints из файла.
var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30,
  'Евгений Прочан', 60, 'Александр Малов', 0];

var hj2 = new StudentList("HJ-2", studentsAndPoints);

//4.Добавить несколько новых студентов в группу. Имена и баллы придумайте самостоятельно.
hj2.add("Михаил Сомов", 95);
hj2.add("Игорь Горовой", 50);
hj2.add("Михаил Горовой", 50);

console.log(hj2);
//5.Создать группу «HTML-7» без студентов и сохранить его в переменной html7. После чего добавить в группу несколько студентов. Имена и баллы придумайте самостоятельно.
var html7 = new StudentList("HTML-7");
console.log(html7);
html7.add("Андрей Фитимов", 50);
html7.add("Екатерина Яровская", 40);
html7.add("Андрей Каляев", 20);
html7.add("Виктория Мирная", 80);

/*6.Добавить спискам студентов метод show, который выводит информацию о группе в следующем виде:
 Группа HTML-7 (2 студентов):
 Студент Наталья Терентьева набрал 10 баллов
 Студент Кирилл Васильев набрал 30 баллов
 Вывести информацию о группах «HJ-2» и «HTML-7».*/

StudentList.prototype.show = function () {
  console.log("Группа %s (%d студентов):", this.groupName, this.length);
  this.forEach(function(student) {
    student.show();
  });
};

hj2.show();
html7.show();

// 7. Перевести одного из студентов из группы «HJ-2» в группу «HTML-7»
StudentList.prototype.moveTo = function (name, targetGroup) {
  var studentIndex = this.findIndex(function (student) {
    return student.name == name;
  });
  if (studentIndex != -1) {
    targetGroup.push(this[studentIndex]);
    this.splice(studentIndex, 1);
  }
};
hj2.moveTo('Ирина Овчинникова', html7);
console.log(html7);
console.log(hj2);

//Дополнительное задание: Добавить спискам студентов метод max, который возвращает студента с наибольшим баллом в этой группе. Изучите документацию метода valueOf в Object.prototype, и попробуйте использовать Math.max для решения этой задачи.
Student.prototype.valueOf = function(){
  return this.point;
}

StudentList.prototype.max = function(){
  var max = Math.max.apply(null, this);
  var maxStudent = this.find(function(student){
    return student.point == max;
  });
  return maxStudent;
};

console.log(hj2.max());

